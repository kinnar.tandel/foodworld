<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_id');
            $table->string('first_name', 50)->index();
            $table->string('last_name', 50)->index();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('phone_no', 25)->nullable()->unique();
            $table->string('avatar')->nullable();
            $table->enum('status', ['Active', 'Inactive', 'Blocked'])->default('Active');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->nullable()->unsigned();
            $table->timestamp('email_verified_at')->nullable();
            //$table->string('name');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
