<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->word,
            'last_name' => $this->faker->word,
            'email' => $this->faker->unique()->safeEmail,
            'username' => str_replace('', ' ', $this->faker->name()),
            'phone_no' => $this->faker->phoneNumber,
            'avatar' => 'noimage.png',
            'status' => 'Active',
            'role_id' => $this->faker->numberBetween(2, 3),
            'created_by' => 1,
            'updated_by' => 1,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10)
        ];
    }
}
