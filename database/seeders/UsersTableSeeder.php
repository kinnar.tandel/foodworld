<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = Role::create(['name' => 'Super Admin']);
        $customer = Role::create(['name' => 'Customer']);
        $restaurant = Role::create(['name' => 'Restaurant']);

        User::create([
            'first_name' => 'Kinnar',
            'last_name' => 'Tandel',
            'email' => 'kinnar.tandel@gmail.com',
            'username' => 'webden',
            'phone_no' => '7874153381',
            'avatar' => 'superadmin.jpg',
            'role_id' => 1,
            'status' => 'Active',
            'created_by' => 1,
            'updated_by' => 1,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        User::factory()->count(99)->create();
    }
}
